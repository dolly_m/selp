Out of the Pan - Food and Cooking

What is it about?
-A website for submitting recipes which get rated by other users and commented on. Basically, a competition website. 
Users are ranked on a voting basis. The user with highest number of votes with all his/her submitted recipes 
is the top ranked user.

Requirements.txt
This is a file containing all requirements which can be found under selp/

Python virtual environment
A python virtual environment named 'venv' was created to install dependencies. 

How to run it?
Open a terminal and type in: cd selp
Then: source <name_of_virtual_environment>/bin/activate  example: source venv/bin/activate
Then: cd recipeProject
Then: python manage.py makemigrations
Followed by: python manage.py migrate
Finally: python manage.py runserver

Go to: http://127.0.0.1:8000/recipeBlog/   
Now you're free to explore.
Once you've registered and signed in, on the top left corner of the homepage you will see hello 'username'! written. 
