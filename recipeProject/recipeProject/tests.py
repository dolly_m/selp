
#testing
from django.test import TestCase
from recipe.models import Recipe __unicode__(self)

class RecipeTest(TestCase):
	def create_recipe(self, name="test recipe", preparationTime="5 minutes", 
	serves="4 people", description="blah blah", ingredients="xyz, abc", directions=" do this, do that", tags="xyz"):
		return Recipe.objects.create(name=name, preparationTime=preparationTime, serves=serves,
					     description=description, ingredients=ingredients, directions=directions)

	def test_recipe_creation(self):
		a = self.create_recipe()
		self.assertTrue(isinstance(a, Recipe))
		self.assertEqual(a.__unicode(), a.name)


