from recipeBlog.models import UserProfile, Recipe, Vote
from django.contrib.auth.models import User
from django import forms

class UserForm(forms.ModelForm):
	username = forms.CharField(widget=forms.TextInput(attrs={'size':40}), help_text="Please enter a username")
	email = forms.CharField(widget=forms.TextInput(attrs={'size':40}), help_text="Please enter your email")
	password = forms.CharField(widget=forms.PasswordInput(attrs={'size':40}), help_text="Please enter a password")

	class Meta:
		model = User
		fields = ['username', 'email', 'password']

class UserProfileForm(forms.ModelForm):
	picture = forms.ImageField(help_text="Select a profile image to upload.", required=False)

	class Meta:
		model = UserProfile
		fields = ['picture']
			
class RecipeForm(forms.ModelForm):
	name = forms.CharField(widget=forms.TextInput(attrs={'size':30}), help_text="Name of Recipe")
	preparationTime = forms.CharField(widget=forms.TextInput(attrs={'size':15}), help_text="Preparation Time")
	serves = forms.CharField(widget=forms.TextInput(attrs={'size':15}), help_text="Serves")
	description = forms.CharField(widget=forms.Textarea(attrs={'cols':50, 'rows':6}), help_text="Description of Recipe")
	ingredients = forms.CharField(widget=forms.Textarea(attrs={'cols':50, 'rows':6}), help_text="Ingredients")
	directions = forms.CharField(widget=forms.Textarea(attrs={'cols':50, 'rows':6}), help_text="Directions")
	

	recipePicture = forms.ImageField(help_text="Photo of recipe(Optional)", required=False)
	class Meta:
		model = Recipe
		fields = ['name', 'preparationTime', 'serves', 'description', 'ingredients', 'directions', 'recipePicture', 'tags']

class VoteForm(forms.ModelForm):
	class Meta:
		model = Vote 
