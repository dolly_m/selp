#from django.shortcuts import HttpResponse
from django.shortcuts import render
from recipeBlog.forms import UserForm, UserProfileForm, RecipeForm, VoteForm
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, DetailView
from recipeBlog.models import Recipe, Vote
from django.views.generic.edit import FormView
from django.contrib.comments.forms import CommentForm
import json

def index(request):
	#return HttpResponse("Out of the pan")
	 # Construct a dictionary to pass to the template engine as its context.
    # Note the key boldmessage is the same as {{ boldmessage }} in the template!
    context_dict = {'boldmessage': "I am bold font from the context"}

    # Return a rendered response to send to the client.
    # We make use of the shortcut function to make our lives easier.
    # Note that the first parameter is the template we wish to use.

    return render(request, 'recipeBlog/index.html', context_dict)

def recipes(request):
	context_dict = {'boldmessage': "I am bold font from the context"}
	return render(request, 'recipeBlog/recipes.html', context_dict)

def register(request):
    context=RequestContext(request)
     
     # A boolean value for telling the template whether the registration was successful.
     # Set to False initially. Code changes value to True when registration succeeds.
    registered = False

     # If it's a HTTP POST, we're interested in processing form data.
    if request.method == 'POST':
        # Attempt to grab information from the raw form information.
        # Note that we make use of both UserForm and UserProfileForm.
        user_form=UserForm(data=request.POST)
        profile_form=UserProfileForm(data=request.POST)

        # If the two forms are valid...
        if user_form.is_valid() and profile_form.is_valid():
            # Save the user's form data to the database.
            user=user_form.save()

            # Now we hash the password with the set_password method.
            # Once hashed, we can update the user object.
            user.set_password(user.password)
            user.save()

            # Now sort out the UserProfile instance.
            # Since we need to set the user attribute ourselves, we set commit=False.
            # This delays saving the model until we're ready to avoid integrity problems.
            profile=profile_form.save(commit=False)
            profile.user=user

            # Did the user provide a profile picture?
            # If so, we need to get it from the input form and put it in the UserProfile model.
            if 'picture' in request.FILES:
                profile.picture=request.FILES['picture']

            # Now we save the UserProfile model instance.
            profile.save()

            # Update our variable to tell the template registration was successful.
            registered=True

            # Invalid form or forms - mistakes or something else?
            # Print problems to the terminal.
            # They'll also be shown to the user.
        else:
            print user_form.errors, profile_form.errors

        # Not a HTTP POST, so we render our form using two ModelForm instances.
        # These forms will be blank, ready for user input.
    else:
        user_form=UserForm()
        profile_form=UserProfileForm()

         # Render the template depending on the context.
    return render_to_response(
            'recipeBlog/register.html',
            {'user_form': user_form, 'profile_form': profile_form, 'registered': registered},
            context)

def please_sign_up(request):
    context_dict = {'boldmessage': "I am bold font from the context"}
    return render(request, 'recipeBlog/please_sign_up.html', context_dict)

def user_login(request):
    # Like before, obtain the context for the user's request.
    context=RequestContext(request)

    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':
        # Gather the username and password provided by the user.
        # This information is obtained from the login form.
        username=request.POST['username']
        password=request.POST['password']

        # Use Django's machinery to attempt to see if the username/password
        # combination is valid - a User object is returned if it is.
        user=authenticate(username=username, password=password)

        # If we have a User object, the details are correct.
        # If None (Python's way of representing the absence of a value), no user
        # with matching credentials was found.
        if user:
            # Is the account active? It could have been disabled.
            if user.is_active:
                # If the account is valid and active, we can log the user in.
                # We'll send the user back to the homepage.
                login(request, user)
                return HttpResponseRedirect('/recipeBlog/')
            else:
                # An inactive account was used - no logging in!
                return HttpResponse("Your OutOfThePan account is disabled!")
        else:
            # Bad login details were provided. So we can't log the user in.
            print "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Invalid login details supplied.")

        # The request is not a HTTP POST, so display the login form.
        # This scenario would most likely be a HTTP GET.
    else:
        # No context variables to pass to the template system, hence the
        # blank dictionary object...
        return render_to_response('recipeBlog/login.html', {}, context)

@login_required
def restrict_access(request):
    return HttpResponse("Since you're logged in, you can see this page!")

# Use the login_required() decorator to ensure only those logged in can access the view.
@login_required
def user_logout(request):
    # Since we know the user is logged in, we can now just log them out.
    logout(request)

    #Take the user back to the homepage
    return HttpResponseRedirect('/recipeBlog/')

def recipe_form(request):
    context=RequestContext(request)

    submitted = False

    if request.method == 'POST':
        # Attempt to grab information from the raw form information.
        # Note that we make use of RecipeForm
        recipe_form=RecipeForm(data=request.POST)

        # If the form is valid...
        if recipe_form.is_valid():
            # Save the user's form data to the database.
            rForm=recipe_form.save()

            # Did the user provide a recipe picture?
            # If so, we need to get it from the input form and put it in the Recipe model.
            if 'recipePicture' in request.FILES:
                profile.picture=request.FILES['recipePicture']

            # Now we save the Recipe model instance.
            rForm.save()

            # Update our variable to tell the template submission was successful.
            submitted=True

            # Invalid form or forms - mistakes or something else?
            # Print problems to the terminal.
            # They'll also be shown to the user.
        else:
            print recipe_form.errors

        # Not a HTTP POST, so we render our form using the ModelForm instance.
        # This form will be blank, ready for user input.
    else:
        recipe_form=RecipeForm()

         # Render the template depending on the context.

    return render_to_response('recipeBlog/recipe_form.html', {'recipe_form': recipe_form, 'submitted': submitted}, context)

class RecipeList(ListView):
    model = Recipe
    queryset = Recipe.with_votes.all()
# Showing the voted state:  indication to know if the headline was voted or not. 
#To achieve this, we can pass ids of all the links that have been voted by the 
#logged in user. This can be passed as a context variable i.e. voted.
    def get_context_data(self, **kwargs):
        context = super(RecipeList, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            voted = Vote.objects.filter(voter=self.request.user)
            recipes_in_page = [recipe.id for recipe in context["object_list"]]
            # like intersection of voted recipes and recipe list on that page
            voted = voted.filter(recipe_id__in=recipes_in_page)
            voted = voted.values_list('recipe_id', flat=True)
            context["voted"] = voted
        return context

class RecipeDetail(DetailView):
    model = Recipe
    
def comment(request):
    return HttpResponseRedirect(request.POST['next'])

def tag_page(request, tag):
    recipes = Recipe.objects.filter(tags__name=tag)
    return render_to_response('recipeBlog/tag_page.html', {'recipes':recipes, 'tag':tag})

#We are using a mixin to implement a JSON response for our AJAX requests
class JSONFormMixin(object):
    def create_response(self, vdict=dict(), valid_form=True):
        response = HttpResponse(json.dumps(vdict), content_type='application/json')
        # successful then 200 
        response.status = 200 if valid_form else 500
        return response

class VoteFormBaseView(FormView):
    form_class = VoteForm

    def create_response(self, vdict=dict(), valid_form=True):
        response = HttpResponse(json.dumps(vdict))
        # successful then 200 
        response.status = 200 if valid_form else 500
        return response
        
        #toggle between the situation: if a user hasn't voted previously, allow vote
        # else delete second vote
    def form_valid(self, form):
        recipe = get_object_or_404(Recipe, pk=form.data["recipe"])
        user = self.request.user
        prev_votes = Vote.objects.filter(voter=user, recipe=recipe)
        has_voted = (len(prev_votes) > 0)

        ret = {"success": 1}
        if not has_voted:
            # add vote
            v = Vote.objects.create(voter=user, recipe=recipe)
            ret["voteobj"] = v.id
        else:
            # delete vote
            prev_votes[0].delete()
            ret["unvoted"] = 1
        return self.create_response(ret, True)

    def form_invalid(self, form):
        ret = {"success": 0, "form_errors": form.errors }
        return self.create_response(ret, False)

class VoteFormView(JSONFormMixin, VoteFormBaseView):
    pass



