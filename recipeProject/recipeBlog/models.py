from django.db import models
from django.contrib.auth.models import User
from taggit.managers import TaggableManager
from django.db.models import Count

# Create your models here.
class  RecipeVoteCountManager(models.Manager):
	def get_query_set(self):
		return super(RecipeVoteCountManager, self).get_query_set().annotate(
			votes = Count('vote')).order_by('-votes')

class  UserProfile(models.Model):
	#This line is required. Links UserProfile to a User model
	user=models.OneToOneField(User)

	# The additional attributes we wish to include.
	picture=models.ImageField(upload_to='profile_images', blank=True)

	# Override the __unicode__() method to return out something meaningful
	def __unicode__(self):
		return self.user.username

class  Recipe(models.Model):
	name = models.CharField(max_length=30, unique=True)
	preparationTime = models.CharField(max_length=20)
	serves = models.CharField(max_length=25)
	description = models.TextField()
	ingredients = models.TextField()
	directions = models.TextField()
	tags = TaggableManager()
	
	recipePicture=models.ImageField(upload_to='recipe_images', blank=True)

	with_votes = RecipeVoteCountManager()
	objects = models.Manager()  #default manager

	def __unicode__(self):
		return self.name

class Vote(models.Model):
	voter = models.ForeignKey(User)
	recipe = models.ForeignKey(Recipe)

	def __unicode__(self):
		return "%s upvoted %s" % (self.voter.username, self.recipe.name)
