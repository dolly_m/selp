from django.conf.urls import patterns, url
from recipeBlog import views
from recipeBlog.views import RecipeList, RecipeDetail
from recipeBlog.models import Recipe
from django.core.urlresolvers import reverse
from recipeBlog.views import VoteFormView

urlpatterns = patterns('',
        url(r'^$', views.index, name='index'),
        url(r'^recipe_list/$', RecipeList.as_view(), name='recipe-list'),
        url(r'^recipe/(?P<pk>\d+)$', RecipeDetail.as_view(), name='recipe-detail'),
        url(r'^register/$', views.register, name='register'),
        url(r'^login/$', views.user_login, name='login'),
        url(r'^register/$', views.register, name='register'),
        url(r'^please_sign_up/$', views.please_sign_up, name='please_sign_up'),
        url(r'^recipe_form/$', views.recipe_form, name='recipe_form'),
        url(r'^restricted/$', views.restrict_access, name='restrict_access'),
        url(r'^logout/$', views.user_logout, name='logout'),
        url(r'^tag_page/(?P<tag>\w+)$', views.tag_page, name='tag-page'), 
        url(r'^vote/$', views.VoteFormView.as_view(), name='vote'), 
	
        )
